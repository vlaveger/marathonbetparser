<?php

declare(strict_types = 1);

namespace MarathonBetParser\Controllers;

use MarathonBetParser\Models\ListOfMatchesInterface;
use MarathonBetParser\Models\DetailOfMatchInterface;
use Http\Request;
use Http\Response;

class SiteController
{
    private $request;
    private $response;
    private $listOfFootballMatches;
    private $config;
    private $detailOfFootballMatch;

    public function __construct(Request $request, Response $response, ListOfMatchesInterface $listOfFootballMatches, DetailOfMatchInterface $detailOfFootballMatch, array $config = [])
    {
        $this->request = $request;
        $this->response = $response;
        $this->listOfFootballMatches = $listOfFootballMatches;
        $this->config = $config;
        $this->detailOfFootballMatch = $detailOfFootballMatch;
    }

    public function showListOfMatches()
    {
        if ($matсhes = $this->listOfFootballMatches->getListOfMatches($this->config['url'])) {
            foreach ($matсhes as $matсh) {
                var_dump($matсh);
            }
        }
        $content = '<h1>Hello World</h1>';
        $content .= 'Hello ' . $this->request->getParameter('name', 'stranger');
        $this->response->setContent($content);
    }
}