<?php declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

$injector = include('Dependencies.php');
$request = $injector->make('Http\HttpRequest');
$response = $injector->make('Http\HttpResponse');

include 'Routing.php';

foreach ($response->getHeaders() as $header) {
    header($header, false);
}

echo $response->getContent();
