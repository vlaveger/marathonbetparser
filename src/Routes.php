<?php

declare(strict_types = 1);

return [
    ['GET', '/', ['\MarathonBetParser\Controllers\SiteController', 'showListOfMatches']],
];