<?php

namespace MarathonBetParser\Models;

use MarathonBetParser\Models\ListOfMatchesInterface;

Class ListOfFootballMatches implements ListOfMatchesInterface
{
    public function getListOfMatches($url) {
        //$html = file_get_contents($url . $page);
        $html = file_get_contents('.././data.html');
        $doc = new \DOMDocument();
        $doc->loadHTML($html);
        $xpath = new \DOMXpath($doc);
        $dataWithHtmlTags = $xpath->query('//table[@class="member-area-content-table "]');
        if ($dataWithHtmlTags->length == 0) {
            return false;
        }
        $result = [];
            foreach ($dataWithHtmlTags as $table) {
                $elementsWithTagA = $table->getElementsByTagName('a');
                $result[] = array(
                            'linkToMatch' => $elementsWithTagA[0]->attributes[1]->value,
                            'team1' => trim($elementsWithTagA[0]->nodeValue),
                            'team2' => trim($elementsWithTagA[1]->nodeValue)
                        );
            }
        return $result;
    }
}
?>
