<?php

namespace MarathonBetParser\Models;

interface ListOfMatchesInterface
{
    public function getListOfMatches(string $url);
}