<?php

namespace MarathonBetParser\Models;

interface DetailOfMatchInterface
{
    public function getDetailOfMatche(string $url);
}
